<?php

$calendar_img_url = get_template_directory_uri() . '/assets/calendar-alt-solid.svg';
$linkedin_img_url = get_template_directory_uri() . '/assets/linkedin-in-brands.svg';
$envelope_img_url = get_template_directory_uri() . '/assets/envelope-regular.svg'; 

?>

<div id="social">
    <a  href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/ronniestevens/60min'});return false;">
        <img class="social-icon" src="<?php echo $calendar_img_url; ?>" alt="Maak een afspraak" />
    </a>
    <a href="https://www.linkedin.com/in/bovanalst/">
        <img class="social-icon" src="<?php echo $linkedin_img_url; ?>" alt="LinkedIn" />
    </a>
    <a href="mailto:info@poespas.nl">
        <img id="popup" class="social-icon" src="<?php echo $envelope_img_url; ?>" alt="Contact" />
    </a>
</div>

